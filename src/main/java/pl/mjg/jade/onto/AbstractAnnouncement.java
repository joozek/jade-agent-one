package pl.mjg.jade.onto;

public class AbstractAnnouncement implements Announcement {
	protected String password;
	protected int offset;

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "Announcement{" +
				"password='" + password + '\'' +
				", offset=" + offset +
				'}';
	}
}

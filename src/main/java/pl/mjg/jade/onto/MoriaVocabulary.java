package pl.mjg.jade.onto;

public interface MoriaVocabulary {
	public static final String ENTER_PASSWORD_PART = "EnterPasswordPart";
	public static final String ENTER_PASSWORD_PART_PASSWORD = "password";
	public static final String ENTER_PASSWORD_PART_OFFSET = "offset";
	public static final String PASSWORD_INFORMATION = "PasswordInformation";
	public static final String PASSWORD_INFORMATION_TYPE = "type";
	public static final String ANNOUNCE = "RequestAnnouncement";
	public static final String VOLUNTARY_ANNOUNCEMENT = "VoluntaryAnnouncement";
	public static final String REPLYING_ANNOUNCEMENT = "ReplyingAnnouncement";
	public static final String ANNOUNCEMENT_PASSWORD = "password";
	public static final String ANNOUNCEMENT_OFFSET = "offset";
	public static final int PASSWORD_INFORMATION_WRONG_PASSWORD = 1;
	public static final int PASSWORD_INFORMATION_OVERLAPPING_PART = 2;
	public static final int PASSWORD_INFORMATION_PART_OK = 3;
	public static final int PASSWORD_INFORMATION_COMPLETE = 4;

	public static final String SERVICE_GUARDIAN = "moria-guardian";
	public static final String SERVICE_RAID = "moria-raid";
}

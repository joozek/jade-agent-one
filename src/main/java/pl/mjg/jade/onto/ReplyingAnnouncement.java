package pl.mjg.jade.onto;

import jade.content.Concept;

public class ReplyingAnnouncement extends AbstractAnnouncement implements Concept {

	public ReplyingAnnouncement() {
	}

	public ReplyingAnnouncement(String password, int offset) {
		this.password = password;
		this.offset = offset;
	}

}

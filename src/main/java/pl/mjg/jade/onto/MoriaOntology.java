package pl.mjg.jade.onto;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.*;

import static pl.mjg.jade.onto.MoriaVocabulary.*;

public class MoriaOntology extends Ontology {
	public static final String NAME = "MORIA_ONTOLOGY";
	private static final MoriaOntology instance = new MoriaOntology();


	public static MoriaOntology getInstance() {
		return instance;
	}

	public MoriaOntology() {
		super(NAME, BasicOntology.getInstance());

		try{
			AgentActionSchema as = new AgentActionSchema(ENTER_PASSWORD_PART);
			add(as, EnterPasswordPart.class);
			as.add(ENTER_PASSWORD_PART_OFFSET, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
			as.add(ENTER_PASSWORD_PART_PASSWORD, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);

			as = new AgentActionSchema(ANNOUNCE);
			add(as, RequestAnnouncement.class);

			ConceptSchema cs = new ConceptSchema(PASSWORD_INFORMATION);
			add(cs, PasswordInformation.class);
			cs.add(PASSWORD_INFORMATION_TYPE, (PrimitiveSchema)getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);

			cs = new ConceptSchema(REPLYING_ANNOUNCEMENT);
			add(cs, ReplyingAnnouncement.class);
			cs.add(ANNOUNCEMENT_PASSWORD, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
			cs.add(ANNOUNCEMENT_OFFSET, (PrimitiveSchema)getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);

			PredicateSchema ps = new PredicateSchema(VOLUNTARY_ANNOUNCEMENT);
			add(ps, VoluntaryAnnouncement.class);
			ps.add(ANNOUNCEMENT_PASSWORD, getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
			ps.add(ANNOUNCEMENT_OFFSET, getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);

		} catch (OntologyException oe) {
			throw new RuntimeException(oe);
		}
	}
}

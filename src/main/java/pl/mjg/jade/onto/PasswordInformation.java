package pl.mjg.jade.onto;

import jade.content.Concept;

import static pl.mjg.jade.onto.MoriaVocabulary.*;

public class PasswordInformation implements Concept {
	private int type;

	public PasswordInformation() {
	}

	public PasswordInformation(int type) {
		setType(type);
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		if(type != PASSWORD_INFORMATION_PART_OK
			&& type != PASSWORD_INFORMATION_OVERLAPPING_PART
			&& type != PASSWORD_INFORMATION_WRONG_PASSWORD
			&& type != PASSWORD_INFORMATION_COMPLETE) {
			throw new IllegalArgumentException("type");
		}
		this.type = type;
	}

	@Override
	public String toString() {
		switch (type) {
			case PASSWORD_INFORMATION_COMPLETE: return "PASSWORD_INFORMATION_COMPLETE";
			case PASSWORD_INFORMATION_OVERLAPPING_PART: return "PASSWORD_INFORMATION_OVERLAPPING_PART";
			case PASSWORD_INFORMATION_PART_OK: return "PASSWORD_INFORMATION_PART_OK";
			case PASSWORD_INFORMATION_WRONG_PASSWORD: return "PASSWORD_INFORMATION_WRONG_PASSWORD";
		}

		throw new AssertionError();
	}
}

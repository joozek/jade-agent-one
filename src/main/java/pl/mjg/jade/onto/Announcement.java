package pl.mjg.jade.onto;

public interface Announcement {
	String getPassword();

	int getOffset();
}

package pl.mjg.jade.onto;

import jade.content.Predicate;

public class VoluntaryAnnouncement extends AbstractAnnouncement implements Predicate {

	public VoluntaryAnnouncement() {
	}

	public VoluntaryAnnouncement(String password, int offset) {
		this.password = password;
		this.offset = offset;
	}

}

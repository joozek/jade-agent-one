package pl.mjg.jade.onto;

import jade.content.AgentAction;

public class EnterPasswordPart implements AgentAction {
	private String password;
	private int offset;

	public EnterPasswordPart() {
	}

	public EnterPasswordPart(String password, int offset) {
		this.password = password;
		this.offset = offset;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
}

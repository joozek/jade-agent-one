package pl.mjg.jade;

import jade.content.Concept;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import pl.mjg.jade.onto.EnterPasswordPart;
import pl.mjg.jade.onto.PasswordInformation;

import static pl.mjg.jade.onto.MoriaVocabulary.*;

public class GuardianAgent extends MoriaAgent {
	public static final String PASSWORD = "alamakota";
	private final char[] currentPassword = new char[PASSWORD.length()];

	@Override
	protected void doSetup() {
		clearCurrentPassword();
		SequentialBehaviour sequentialBehaviour = new SequentialBehaviour();
		sequentialBehaviour.addSubBehaviour(new RegisterInDf());
		sequentialBehaviour.addSubBehaviour(new Listener());
		addBehaviour(sequentialBehaviour);
	}

	private void clearCurrentPassword() {
		for (int i = 0; i < currentPassword.length; i++) {
			currentPassword[i] = 0;
		}
	}

	private class Listener extends CyclicBehaviour {
		@Override
		public void action() {
			System.out.println("listening for messages");
			ACLMessage message = receive();
			if (message == null) {
				block();
				return;
			}

			System.out.println("received message");
			try {
				Action content = (Action) getContentManager().extractContent(message);
				Concept action = content.getAction();
				if(action instanceof EnterPasswordPart) {
					EnterPasswordPart command = (EnterPasswordPart) action;
					handleEpp(message, content, command);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		private void handleEpp(ACLMessage message, Action content, EnterPasswordPart epp) throws Codec.CodecException, OntologyException {
			System.out.println(String.format("received EPP (%d): %s", epp.getOffset(), epp.getPassword()));
			if(epp.getOffset() + epp.getPassword().length() > PASSWORD.length()) {
				System.out.println("bad password. offfset="+epp.getOffset()+" len="+epp.getPassword().length());
				clearCurrentPassword();
				sendPasswordInformationReply(message, content, PASSWORD_INFORMATION_OVERLAPPING_PART);
				return;
			}
			for(int i = epp.getOffset(); i < epp.getPassword().length(); ++i) {
				if(currentPassword[i] != 0) {
					System.out.println("overlapping password on pos "+i);
					System.out.println("current password was "+new String(currentPassword).replace('\0', '_'));
					clearCurrentPassword();
					sendPasswordInformationReply(message, content, PASSWORD_INFORMATION_OVERLAPPING_PART);
					return;
				}
			}
			epp.getPassword().getChars(0, epp.getPassword().length(), currentPassword, epp.getOffset());
			if(isPasswordComplete()) {
				if(PASSWORD.equals(new String(currentPassword))) {
					System.out.println("OKOKOKOKOKOK");
					System.out.println("OKOKOKOKOKOK");
					System.out.println("OKOKOKOKOKOK");
					System.out.println("OKOKOKOKOKOK");
					sendPasswordInformationReply(message, content, PASSWORD_INFORMATION_COMPLETE);
				} else {
					System.out.println("WRONG PASSWORD");
					sendPasswordInformationReply(message, content, PASSWORD_INFORMATION_WRONG_PASSWORD);
				}
				clearCurrentPassword();
			} else {
				sendPasswordInformationReply(message, content, PASSWORD_INFORMATION_PART_OK);
			}

		}

	}

	private boolean isPasswordComplete() {
		boolean isPasswordComplete = true;
		for (char c : currentPassword) {
			if(c == 0) {
				isPasswordComplete = false;
				break;
			}
		}
		return isPasswordComplete;
	}

	private void sendPasswordInformationReply(ACLMessage message, Action originalContent, int infomationType) throws Codec.CodecException, OntologyException {
		sendReply(message, originalContent, ACLMessage.INFORM, new PasswordInformation(infomationType));
	}

	private class RegisterInDf extends OneShotBehaviour{
		@Override
		public void action() {
			ServiceDescription sd = new ServiceDescription();
			sd.setType(SERVICE_GUARDIAN);
			sd.setName(getName());
			sd.setOwnership("joozek");
			DFAgentDescription agentDescription = new DFAgentDescription();
			agentDescription.setName(getAID());
			agentDescription.addServices(sd);
			try {
				DFAgentDescription[] currentServers = DFService.search(myAgent, agentDescription);
				if (currentServers.length > 0) {
					DFService.deregister(myAgent, agentDescription);
				}
				DFService.register(myAgent, agentDescription);
				System.out.println(String.format("%s registered", getName()));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}
}

package pl.mjg.jade;

import jade.content.AgentAction;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import pl.mjg.jade.onto.MoriaOntology;
import pl.mjg.jade.onto.MoriaVocabulary;

public abstract class MoriaAgent extends Agent{

	protected final SLCodec codec = new SLCodec();
	protected final MoriaOntology ontology = MoriaOntology.getInstance();

	@Override
	protected final void setup() {
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);

		doSetup();
	}

	protected abstract void doSetup();

	protected void sendRequest(AgentAction content, AID receiver) {
		ACLMessage message1 = new ACLMessage(ACLMessage.REQUEST);
		message1.setLanguage(codec.getName());
		message1.setOntology(ontology.getName());
		try {
			getContentManager().fillContent(message1, new Action(receiver, content));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		ACLMessage message = message1;
		message.addReceiver(receiver);
		send(message);
	}

	protected AID findServer() {
		try {
			DFAgentDescription[] servers = findAllServiceProviders(MoriaVocabulary.SERVICE_GUARDIAN);
			return servers[0].getName();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected DFAgentDescription[] findAllServiceProviders(String serviceId) throws FIPAException {
		ServiceDescription serviceDescription = new ServiceDescription();
		serviceDescription.setType(serviceId);
		DFAgentDescription agentDescription = new DFAgentDescription();
		agentDescription.addServices(serviceDescription);

		return DFService.search(this, agentDescription);
	}

	protected void pr(String format, Object... params) {
		System.out.println(nameOfThisClass() +": "+String.format(format, params));
	}

	private String nameOfThisClass() {
		String name = this.getClass().getName();
		return name.substring(name.lastIndexOf('.') + 1);
	}

	protected void sendReply(ACLMessage originalMessage, Action originalContent, int performative, Object replyContent) {
		try {
			ACLMessage reply = originalMessage.createReply();
			reply.setPerformative(performative);
			Result result = new Result(originalContent, replyContent);
			getContentManager().fillContent(reply, result);
			send(reply);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

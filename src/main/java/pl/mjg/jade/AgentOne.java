package pl.mjg.jade;

import jade.content.ContentManager;
import jade.content.lang.sl.SLCodec;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class AgentOne extends Agent {
    @Override
    protected void setup() {
        super.setup();

		ContentManager contentManager = getContentManager();
		contentManager.registerLanguage(new SLCodec());
//		contentManager.registerOntology(new Ontology());

		addBehaviour(new Listener());
    }

	private class Listener extends CyclicBehaviour {

		private AgentOne.Speaker currentSpeaker;

		@Override
		public void action() {
			ACLMessage message = receive(MessageTemplate.MatchContent("knock-knock"));
			if(message != null) {
				System.out.println("received message");
				if(currentSpeaker == null) {
					turnOn(message.getSender());
				} else {
					turnOff();
				}
			} else {
				block();
			}
		}

		private void turnOn(AID sender) {
			System.out.println("turning on");
			currentSpeaker = new Speaker(1000, sender);
			addBehaviour(currentSpeaker);
		}

		private void turnOff() {
			System.out.println("turning off");
			currentSpeaker.end();
		}
	}

	private class Speaker extends SimpleBehaviour {

		private boolean done = false;

		private final long timeout;
		private AID target;
		private long wakeupTime;

		private Speaker(long timeout, AID target) {
			this.timeout = timeout;
			this.target = target;
		}

		@Override
		public void onStart() {
			scheduleNextWakeup();
		}

		private void scheduleNextWakeup() {
			wakeupTime = System.currentTimeMillis() + timeout;
		}

		@Override
		public void action() {
			long dt = wakeupTime - System.currentTimeMillis();
			if(dt <= 0) {
				speak();
				scheduleNextWakeup();
			} else {
				block(dt);
			}
		}

		private void speak() {
			System.out.println("ping");
			ACLMessage message = new ACLMessage(ACLMessage.INFORM);
			message.addReceiver(target);
			message.setContent("pong");
			send(message);
		}

		@Override
		public boolean done() {
			return done;
		}

		private void end() {
			done = true;
		}
	}


}

package pl.mjg.jade;

import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import pl.mjg.jade.onto.*;

public class StrangerAgent extends MoriaAgent {

	private final String passwordPart;
	private int offset;
	private boolean[] knowledgeMask;

	public StrangerAgent(String passwordPart) {
		knowledgeMask = new boolean[getRealPassword().length()];
		this.passwordPart = passwordPart;
        this.offset = getRealPassword().indexOf(passwordPart);
		markAsKnown(passwordPart, offset);
	}

	private String getRealPassword() {
		return GuardianAgent.PASSWORD;
	}

	@Override
	protected void doSetup() {
		SequentialBehaviour sequentialBehaviour = new SequentialBehaviour(this);
		sequentialBehaviour.addSubBehaviour(new BroadcastAnnouncement());
		sequentialBehaviour.addSubBehaviour(new RegisterInDf());
		sequentialBehaviour.addSubBehaviour(new SendAnnouncementRequest());
		sequentialBehaviour.addSubBehaviour(new ListenForMoriaMessages());
		sequentialBehaviour.addSubBehaviour(new StartListener());
		addBehaviour(sequentialBehaviour);
	}

	@Override
	protected void takeDown() {
		DFAgentDescription agentDescription = GetRaidingAgentDescription();
		try {
			DFService.deregister(this, agentDescription);
			pr("Deregistered");
		} catch (FIPAException e) {
			pr("Failed to deregister service. cause: %s", e);
		}
	}

	private DFAgentDescription GetRaidingAgentDescription() {
		ServiceDescription serviceDescription = new ServiceDescription();
		serviceDescription.setType(MoriaVocabulary.SERVICE_RAID);
		serviceDescription.setName(getName());
		DFAgentDescription agentDescription = new DFAgentDescription();
		agentDescription.addServices(serviceDescription);
		return agentDescription;
	}

	private class SendPassword extends OneShotBehaviour {
		@Override
		public void action() {
			EnterPasswordPart enterPasswordPart = new EnterPasswordPart(passwordPart, offset);
			sendRequest(enterPasswordPart, findServer());
			pr("passwordPart sent");
			addBehaviour(new WaitForResponse());
		}
	}

	private class StartListener extends CyclicBehaviour {
		@Override
		public void action() {
			ACLMessage message = receive(MessageTemplate.MatchContent("aa"));
			if(message == null) {
				block();
				return;
			}

			addBehaviour(new SendPassword());
		}
	}

	private class WaitForResponse extends SimpleBehaviour{
		private boolean done = false;

		@Override
		public void action() {
			ACLMessage message = receive(MessageTemplate.MatchSender(findServer()));
			if (message == null) {
				block();
				return;
			}

			try {
				if (message.getPerformative() != ACLMessage.INFORM) {
					throw new RuntimeException("Bad performative!");
				}
				Result result = (Result) getContentManager().extractContent(message);
				PasswordInformation passwordInformation = (PasswordInformation) result.getValue();
				pr("response: " + passwordInformation.toString());
				done = true;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public boolean done() {
			return done;
		}
	}

	private class RegisterInDf extends OneShotBehaviour {
		@Override
		public void action() {
			DFAgentDescription agentDescription = GetRaidingAgentDescription();
			try {
				DFService.register(myAgent, agentDescription);
			} catch (FIPAException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private class ListenForMoriaMessages extends CyclicBehaviour {
		@Override
		public void action() {
			ACLMessage message = receive(MessageTemplate.MatchOntology(MoriaOntology.NAME));
			if (message == null || message.getSender().equals(getAID())) {
				block();
				return;
			}

			try {
				ContentElement content = getContentManager().extractContent(message);
				if(message.getPerformative() == ACLMessage.REQUEST) {
					Action action = (Action)content;
					addBehaviour(new ReplyWithAnnouncement(message, action));
				} else if(message.getPerformative() == ACLMessage.INFORM) {
					handleAnnouncement((Announcement)content);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void handleAnnouncement(Announcement announcement) {
		markAsKnown(announcement.getPassword(), announcement.getOffset());
		if (allIsKnown()) {
			pr("all is known");
			addBehaviour(new SendPassword());
		}
	}

	private boolean allIsKnown() {
		boolean allIsKnown = true;
		for (boolean b : knowledgeMask) {
			if(!b) {
				allIsKnown = false;
				break;
			}
		}

		return allIsKnown;
	}

	private void markAsKnown(String password, int offset) {
		if(offset+password.length() > getRealPassword().length()) {
			die("supplied password too long");
		}
		for (int i = offset; i < password.length()+offset; i++) {
			 knowledgeMask[i] = true;
		}
	}

	private void die(String s) {
		pr(s);
		doDelete();
	}

	private class ReplyWithAnnouncement extends OneShotBehaviour {
		private ACLMessage originalMessage;

		public ReplyWithAnnouncement(ACLMessage originalMessage, Action value) {
			this.originalMessage = originalMessage;
		}

		@Override
		public void action() {

			try {
				ACLMessage message = originalMessage.createReply();
				message.setPerformative(ACLMessage.INFORM);
				message.setOntology(MoriaOntology.NAME);
				message.setLanguage(codec.getName());
				getContentManager().fillContent(message, new VoluntaryAnnouncement(passwordPart, offset));
				send(message);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private class BroadcastAnnouncement extends OneShotBehaviour {
		@Override
		public void action() {
			try {
				ACLMessage message = new ACLMessage(ACLMessage.INFORM);
				sendAnnouncementInMessage(message);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void sendAnnouncementInMessage(ACLMessage message) throws FIPAException, Codec.CodecException, OntologyException {
		message.setPerformative(ACLMessage.INFORM);
		message.setOntology(MoriaOntology.NAME);
		message.setLanguage(codec.getName());
		pr("Showing raiders:");
		for (DFAgentDescription agentDescription : findAllServiceProviders(MoriaVocabulary.SERVICE_RAID)) {
			if (!agentDescription.getName().equals(getAID())) {
				message.addReceiver(agentDescription.getName());
				pr(agentDescription.getName().toString());
			}
		}
		getContentManager().fillContent(message, new VoluntaryAnnouncement(passwordPart, offset));
		send(message);
	}

	private class SendAnnouncementRequest extends OneShotBehaviour {
		@Override
		public void action() {
			RequestAnnouncement requestAnnouncement = new RequestAnnouncement();
			try {
				for (DFAgentDescription agentDescription : findAllServiceProviders(MoriaVocabulary.SERVICE_RAID)) {
					sendRequest(requestAnnouncement, agentDescription.getName());
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

}
